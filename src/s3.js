import S3 from 'aws-sdk/clients/s3.js'
import dotenv from 'dotenv-safe'
import * as upload from './upload.js'
import mime from 'mime-types'

dotenv.config()

const s3 = new S3({})
const bucket = process.env.AWS_BUCKET_NAME


async function write(stream) {
    return new Promise((resolve, reject) => {
        const id = `${upload.encryptID()}.${mime.extension(stream.mimeType)}`
        const params = {
            Bucket: bucket,
            Key: id,
            Body: stream,
            ContentType: stream.mimeType,
            StorageClass: 'GLACIER_IR'
        }
        s3.upload(params, (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve({ id })
            }

        })
    })
}

async function read(key) {

    let head
    try {
        head = await getHead(key)
    } catch (e) {
        throw new Error('File not found')
    }

    const param = {
        Bucket: bucket,
        Key: key
    }
    const stream = s3.getObject(param).createReadStream()
    return [stream, head]

}

async function getHead(key) {
    const param = {
        Bucket: bucket,
        Key: key
    }
    s3.headObject(param, (err, data) => {
        if (err) throw err
        return data
    })
}

async function delObject(key) {
    const param = {
        Bucket: bucket,
        Key: key
    }
    s3.deleteObject(param, (err, data) => {
        if (err) throw err
        return data
    })
}

async function deepWrite(stream) {
    return new Promise((resolve, reject) => {
        const id = `${upload.encryptID()}.${mime.extension(stream.mimeType)}`
        const params = {
            Bucket: bucket,
            Key: id,
            Body: stream,
            ContentType: stream.mimeType,
            StorageClass: 'DEEP_ARCHIVE'
        }
        s3.upload(params, (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve({ id })
            }

        })
    })
}

async function deepTransfer(file, head, id) {
    return new Promise((resolve, reject) => {
        const params = {
            Bucket: bucket,
            Key: id,
            Body: file,
            ContentType: head.mimeType,
            StorageClass: 'DEEP_ARCHIVE'
        }
        s3.upload(params, (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve({ id })
            }

        })
    })
}

async function fromDeepArchive(DA_bucket, bucket, key) {
    return new Promise((resolve, reject) => {
        const params = {
            Bucket: DA_bucket,
            Key: key,
            Tier: "Standard",
            RestoreRequest: {
                outputLocation: {
                    S3: {
                        BucketName: bucket,
                        Prefix: "AGIMAT-DA-",
                        StorageClass: "GLACIER_IR"
                    },
                }
            },
        }
        s3.restoreObject(params, (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve({ id })
            }

        })
    })
}


module.exports = {
    write,
    read,
    getHead,
    delObject,
    deepWrite,
    deepTransfer,
    fromDeepArchive
}