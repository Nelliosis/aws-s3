import * as upload from './upload.js'

export default function checkFileSize() {
    return async function (ctx, next) {
        const contentLength = ctx.headers['content-length']
        if (contentLength && upload.reachedSizeLimit(contentLength)) {
            ctx = Object.assign(ctx, {
                status: 413,
                body: { message: 'Size limit reached' }
            })
            return
        }
        return await next()
    }
}

