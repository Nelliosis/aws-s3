import path from 'path'
import fs from 'fs'
import crypto from 'crypto'
import base64 from 'base64url'
import dotenv from 'dotenv-safe'
import mime from 'mime-types'

dotenv.config()

const fileTypes = process.env.ACCEPTED_FILE_TYPES.split(',') || ['image/jpg', 'image/jpeg', 'image/png', 'image/tiff', 'application/pdf']
const sizeLimit = parseInt(process.env.SIZE_LIMIT) || 3500000


function reachedSizeLimit(size) {
    return sizeLimit !== 0 && size > sizeLimit
}

async function writeToDisk(directory, stream) {
    if (!fs.existsSync(path.stream)) return false

    const id = `${upload.makeID()}.${mime.extension(stream.mimeType)}`
    const out = fs.createWriteStream(path.join(directory, id))
    stream.pipe(out)

    out.on('error', (err) => {
        console.err(err)
    })
    out.on('finish', () => {
        const { size } = fs.statSync(stream.path)
        if (reachedSizeLimit(size)) {
            fs.unlink(directory, id)
            return { message: "File size limit reached" }
        }
        if (fs.existsSync(path.stream)) {
            fs.unlink(stream.path, () => { })
        }
        return { id }

    })

}

async function readFromDisk(directory, name) {

    const location = path.join(directory, name)
    const stats = path.statSync(location)
    if (!stats) throw new error('File not found or doesn\'t exist')

    const read = fs.createReadStream(location)
    return [read, stats]
}

async function unlink(directory, name) {
    const location = path.join(directory, name)
    fs.unlink(location, err => {
        if (err) throw new error(err)
    })
}

function checkID(id) {
    try {
        base64.decode(id)
        return true
    } catch (e) {
        return false
    }
}

function encryptID() {
    return base64.encode(crypto.randomBytes(12))
}

function isAWS() {
    return process.env.SOURCE === 'AWS'
}

module.exports = {
    fileTypes,
    sizeLimit,
    reachedSizeLimit,
    writeToDisk,
    readFromDisk,
    unlink,
    checkID,
    encryptID,
    isAWS

}