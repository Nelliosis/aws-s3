const pg = require('../pgconfig')


async function selectMediaType(id) {
    return pg('uploads').select('media_type').where('id', id)
}

async function insert(data) {
    return pg('uploads').insert([{
        id: data.id,
        subject: data.subject,
        media_type: data.media_type,
        created_at: data.created_at,
        expires_at: data.expires_at
    }])
}

async function updateExpiry(id) {
    return pg('uploads').update([{ expires_at: 0 }]).where('id', id)
}

async function selectExpiry(now) {
    return pg('uploads').select('id').where(() => {
        this.where('expires_at', '!=', 0).andWhere('expires_at', '<', now)
    })
}

async function deleteRow(id) {
    return pg('uploads').where('id', id).del()
}

module.exports = {
    selectMediaType,
    insert,
    updateExpiry,
    selectExpiry,
    deleteRow
}