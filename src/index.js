import Koa from 'koa'
import Router from 'koa-router'
import dotenv from 'dotenv-safe'
import moment from 'moment'
import busboy from 'async-busboy'

import * as upload from './upload.js'
import * as pg from './postgres.js'
import * as s3 from './s3.js'
import checkFileSize from './filesize.js'

dotenv.config()
const app = new Koa()
const port = 8000

const router = new Router()

    .post('/', checkFileSize(), async (ctx) => {

        const { files } = await busboy(ctx.req)
        if (files.length !== 1) {
            ctx.status = 400
            ctx.body = { message: 'One file per upload only.' }
            return
        }

        const file = files[0]
        if (!process.env.ACCEPTED_FILE_TYPES.includes(file.mimeType)) {
            ctx.status = 400
            ctx.body = { message: 'invalid file type' }
            return
        }

        let result
        if (upload.isAWS()) {
            result = await s3.write(file)
        } else {
            result = await upload.writeToDisk(file)
        }

        if (result.message) {
            Object.assign(ctx, { status: 413, body: { message: result.message } })
        }

        console.log(result.id)

        const { id } = result
        if (!id) {
            ctx.status = 404
            ctx.body = { message: 'file not found' }
            return
        }

        const data = {
            id: id,
            subject: ctx.subject || '',
            media_type: file.mimeType,
            created_at: moment().unix(),
            expires_at: moment().add(24, 'hours').unix()
        }
        await pg.insert(data)

        ctx.status = 200
        ctx.body = { fieldName: file.fieldName, location: `${process.env.BASE_URI}/${id}` }

    })

    .get('/:id', async (ctx) => {
        if (!upload.validID(ctx.params.id)) {
            ctx.status = 400
            ctx.body = { message: 'invalid ID or bad request' }
            return
        }

        const { rows } = await pg.selectMediaType(ctx.params.id)
        if (rows === 0) {
            ctx.status = 400
            ctx.body = { message: 'file not found' }
            return
        }

        if (upload.isAWS()) {
            const [file, head] = await s3.read(ctx.params.id)
            ctx.body = file
            ctx.length = head.ContentLength
            ctx.type = head.ContentType
        } else {
            const [file, head] = await upload.readFromDisk(ctx.params.id)
            ctx.body = file
            ctx.length = stats.size
            ctx.type = rows[0].media_type
        }
        ctx.status = 200

    })

    .del('/:id', async (ctx) => {

        if (!upload.validID(ctx.params.id)) {
            ctx.status = 400
            ctx.body = { message: 'invalid ID or bad request' }
            return
        }

        const response = await pg.deleteRow(ctx.params.id)
        if (response.rowCount === 0) {
            ctx.status = 400
            ctx.body = { message: 'file not found' }
            return
        }

        try {
            if (upload.isAWS()) await s3.delObject(ctx.params.id)
            else await upload.unlink(process.env.UPLOAD_DIR, ctx.params.id)
        } catch (e) {
            ctx.status = 400
            ctx.body = { message: 'file not found' }
            return
        }

        ctx.status = 204

    })

    .post('/:id/commit', async (ctx) => {

        if (!upload.validID(ctx.params.id)) {
            ctx.status = 400
            ctx.body = { message: 'invalid ID or bad request' }
            return
        }

        const response = await pg.updateExpiry(ctx.params.id)
        if (response.rowCount === 0) {
            ctx.status = 400
            ctx.body = { message: 'file not found' }
            return
        }

        ctx.status = 204

    })

    .post('/gc', async (ctx) => {

        let files = []
        pg.transaction(async (tx) => {
            const { rows } = tx.selectExpiry(moment().unix())

            for (const row of rows) {
                const id = row.id
                try {
                    await upload.unlink(process.env.UPLOAD_DIR, row.id)
                } catch (e) {
                    console.warn(`unlink failed at ${id}`)
                    continue
                }
                files.push(id)
            }

            for (const id of files) {
                await tx.deleteRow(id)
            }

        })

        ctx.status = 200
        ctx.body = { files: files }
    })

    .post('/deepWrite', checkFileSize(), async (ctx) => {
        const { files } = await busboy(ctx.req)
        if (files.length !== 1) {
            ctx.status = 400
            ctx.body = { message: 'One file per upload only.' }
            return
        }

        const file = files[0]
        if (!process.env.ACCEPTED_FILE_TYPES.includes(file.mimeType)) {
            ctx.status = 400
            ctx.body = { message: 'invalid file type' }
            return
        }

        let result
        if (upload.isAWS()) {
            result = await s3.deepWrite(file)
        } else {
            result = await upload.writeToDisk(file)
        }

        if (result.message) {
            Object.assign(ctx, { status: 413, body: { message: result.message } })
        }

        console.log(result.id)

        const { id } = result
        if (!id) {
            ctx.status = 404
            ctx.body = { message: 'file not found' }
            return
        }

        const data = {
            id: id,
            subject: ctx.subject || '',
            media_type: file.mimeType,
            created_at: moment().unix(),
            expires_at: moment().add(24, 'hours').unix()
        }
        await pg.insert(data)

        ctx.status = 200
        ctx.body = { fieldName: file.fieldName, location: `${process.env.BASE_URI}/${id}` }
    })

    .get('/:id/deepTransfer', async (ctx) => {

        if (!upload.validID(ctx.params.id)) {
            ctx.status = 400
            ctx.body = { message: 'invalid ID or bad request' }
            return
        }

        const { rows } = await pg.selectMediaType(ctx.params.id)
        if (rows === 0) {
            ctx.status = 400
            ctx.body = { message: 'file not found' }
            return
        }

        let result
        if (upload.isAWS()) {
            const [file, head] = await s3.read(ctx.params.id)
            result = await s3.deepTansfer(file, head, ctx.params.id)
        } else {
            const [file, head] = await upload.readFromDisk(ctx.params.id)
            result = await upload.writeToDisk(file)
        }

        if (result.message) {
            Object.assign(ctx, { status: 413, body: { message: result.message } })
        }

        const { id } = result
        if (!id) {
            ctx.status = 404
            ctx.body = { message: 'file missing. Aborted' }
            return
        }

        const data = {
            id: id,
            expires_at: moment().add(24, 'hours').unix()
        }
        await pg.updateExpiry(data)

        ctx.status = 200
        ctx.body = { message: `${id} transferred to Deep Archive` }


    })

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(port)
console.log(`Listening at port: ${port}`)