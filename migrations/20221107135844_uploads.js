/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
    return knex.schema.createTable('uploads', table => {
        table.text('id').primary()
        table.text('subject').notNullable()
        table.text('media_type').notNullable()
        table.bigInteger('created_at')
        table.bigInteger('expires_at')
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
    return knex.schema.dropTableIfExists('uploads')
};
