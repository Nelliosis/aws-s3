FROM node:19-alpine3.15

RUN apk add --update --no-cache tzdata \
    && cp /usr/share/zoneinfo/Asia/Manila /etc/localtime \
    && echo "Asia/Manila" > /etc/timezone \
    && apk del tzdata

WORKDIR /app

ENV UPLOAD_DIR ./upload

RUN mkdir -p ${UPLOAD_DIR}

COPY . .

EXPOSE 8000
CMD ["/app/wait-for.sh", "postgres:5432", "--", "/app/start.sh"]
