const moment = require('moment')

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('uploads').del()
  await knex('uploads').insert([
    {
      id: "test",
      subject: "a test",
      media_type: "jpg",
      created_at: moment().unix(),
      expires_at: moment().add(24, 'hours').unix()
    }
  ]);
};
