// Update with your config settings.

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
module.exports = {

  development: {
    client: 'postgresql',
    connection: process.env.DB_URL,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: 'migrations',
      tableName: 'knex_migrations'
    },
    seeds: {
      directory: 'seeds'
    }
  },

  staging: {
    client: 'postgresql',
    connection: process.env.DB_URL,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: 'migrations',
      tableName: 'knex_migrations'
    },
    seeds: {
      directory: 'seeds'
    }
  },

  production: {
    client: 'postgresql',
    connection: process.env.DB_URL,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: 'migrations',
      tableName: 'knex_migrations'
    },
    seeds: {
      directory: 'seeds'
    }
  }

};
